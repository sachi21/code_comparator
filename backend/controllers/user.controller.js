const dotenv = require('dotenv').config();
const bcrypt = require("bcryptjs");
const User = require('../models/user.model.js');
const jwt = require('jsonwebtoken');

const signUp = async (req, res) => {
    const { email, password } = req.body
    console.log(email);
    console.log(password);
    //check if both email and password are provided
    if (!email || !password) return res.status(400).send('Please enter all fields')

    // check if password is at least 6 characters long and follows the password requirements
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d@!#%&_-]{6,}$/
    if (!passwordRegex.test(password)) return res.status(400).send('Password must be at least 6 characters long and contain at least one uppercase letter, one lowercase letter and one number')
    
    
    // check if email is of correct format
    const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    if(!emailRegex.test(email)) return res.status(400).send('Please enter a valid email address')
    
    try {
        const hashedPassword = await bcrypt.hash(password, 12)

        const result = new User({
            email,
            password: hashedPassword
        })

        await result.save()

        res.status(201).send(result)
    } catch (error) {
        res.status(500).json({ message: "Something went wrong" })
    }
}

const login = async (req, res) => {
    const { email, password } = req.body
    try{
        console.log(email)
        const user = await User.findOne({ email })
        if (!user) return res.status(404).send('User not found')

        const isPasswordCorrect = await bcrypt.compare(password, user.password)
        if (!isPasswordCorrect) return res.status(400).send('Invalid credentials')

        const token = jwt.sign({ email: user.email, id: user._id }, process.env.SECRET, { expiresIn: "1h" })

        res.status(200).send({
            user,
            token
        })

    } catch (error) {
        res.status(500).json({ message: "Something went wrong" })
    }
}


module.exports = { signUp, login };