const dotenv = require('dotenv').config();
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const cors = require('cors');
const _ = require('lodash');

const projectsRoutes = require('./routes/project.routes');
const userRoutes = require('./routes/user.routes.js');

const User = require('./models/user.model');
const Project = require('./models/project.model');

// cors
app.use(cors());

//allow request methods
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ extended: true, limit: '50mb' }))

// Test Server Health Check
app.get('/', (req, res) => {
    res.send('Server is up and running');
});

app.use('/projects', projectsRoutes);
app.use('/users', userRoutes);

// start server
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
    mongoose.connect(process.env.DB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(async () => {
        console.log('Connected to MongoDB');
        await User.init();
        await Project.init();
    }
    ).catch((err) => {
        console.log(err);
    });
});


