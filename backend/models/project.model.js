const { Schema, model } = require('mongoose');

const projectSchema = new Schema({
    uid: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        required: true,
    },
    files: Object,
    shareable: {
        type: Boolean,
        default: false,
    }
});

module.exports = model('Project', projectSchema);