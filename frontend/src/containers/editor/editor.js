import React, { useRef } from "react";
import "./editor.css";

import { DiffEditor } from "@monaco-editor/react";
import { BsUpload } from 'react-icons/bs';
import { AiFillFolderOpen } from 'react-icons/ai';
import { BiDownload } from 'react-icons/bi';


function EditorPage() {
    const diffEditorRef = useRef(null);
    
    function handleEditorDidMount(editor, monaco) {
        diffEditorRef.current = editor;
    }

    return (
        <div className="Editor">
            <div className="main">
                <div className="side-panel">
                    <div>
                        <button><BsUpload size={18}/>Upload file/folder</button>
                        <button className="secondary"><AiFillFolderOpen size={18}/>Folder Outline</button>
                    </div>
                    <button><BiDownload size={18}/>Save to Projects</button>
                </div>
                <DiffEditor
                    height="calc(100vh - 70px)"
                    className="diff-editor"
                    language="javascript"
                    original="// the original code"
                    modified="// the modified code"
                    theme="vs-dark"
                    onMount={handleEditorDidMount}
                />
            </div>
        </div>
    )
}

export default EditorPage;
