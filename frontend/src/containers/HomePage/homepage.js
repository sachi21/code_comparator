import React from 'react';
import './homepage.css'
import homepageimg from '../../assets/homepageimg.png'
import team from '../../assets/about team.png'
import Bullet from '../../assets/bullet.svg'
import Button from '../../components/button/button';

function HomePage() {

  return (
    <div className="Homepage">
      <div className="main">
        <div className="left">
          &lt;CODECOMPARE&gt;<br />
          <div className="tagline">comparing codes made easier</div>
          <Button text={"Go to Editor"} />
        </div>
        <div className="right">
          <img className="image" src={homepageimg} />
        </div>
      </div>

      <div id="how-it-works" className="two">
        <h1>HOW IT WORKS?</h1>
        <div className='boxes'>
          <div className="box">
            <img src={Bullet} width="20px" />
            <h5>Upload your Project</h5>
            <p>Select the file you are currently working on</p>
          </div>
          <div className="box left-box">
            <div className='reverse'>
              <img src={Bullet} width="20px" />
              <h5>Paste Code</h5>
            </div>
            <p>Select the file you are currently working on</p>
          </div>
          <div className="box">
            <img src={Bullet} width="20px" />
            <h5>Compare Codes</h5>
            <p>Select the file you are currently working on</p>
          </div>
        </div>
      </div>

      <div id="about-us" className="three">
        <h1>About Our Team</h1>
        <div className='team-container'>
          <img className="team" src={team} />
          <div className='box'>
            <div></div>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fringilla facilisis mi, laoreet pellentesque ipsum maximus ut. Aenean vitae porttitor ex. Ut a est sit amet lacus suscipit fermentum non sit amet est.</p>
          </div>
        </div>
      </div>

    </div>
  );
}

export default HomePage;