import React from 'react';
import './login.css';
import Key from '../../assets/keyimg.png';
import Button from '../../components/button/button';

function Login(){
    return(
        <div className="PageTemplate">
            <div className="Form">
                <form>
                    <h2 className="LoginHeader">Login</h2>
                    <input type="text" className="username" placeholder="Username"></input>
                    <input type="password" className="password" placeholder="Password"></input>
                    <Button text="Login"/>
                    <p className="Para">Don't have an account?  <a href="/signup">Sign up Now</a></p>
                </form>
            </div>
            <div class= " loginimage">
                <img class="keyimg" src={Key}/>
            </div>
        </div>
    )
}


export default Login;