import React from "react";
import "./projects.css";

function Projects() {
  return (
    <div className="Projects">
      <h1>Projects</h1>
      <h4>Lorem ipsum dolor sit amet</h4>
      <div className="wrapper">
        {[...Array(10)].map((x, i) =>
          <Card
            title={"Project " + i}
            description={"Description "+i}
          />
        )}
      </div>
    </div>
  );
}

function Card(props) {
  return (
    <div className="card">
      <div className="card__body">
        <h2 className="card__title">{props.title}</h2>
        <p className="card__description">{props.description}</p>
      </div>
    </div>
  );
}

export default Projects;
