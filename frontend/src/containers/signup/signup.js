import React from 'react';
import './signup.css';
import Key from '../../assets/keyimg.png';
import Button from '../../components/button/button';

function SignUp(){
    return(
        <div className="PageTemplate">
            <div className="Form">
                <form>
                    <h2 className="SignUpHeader">SignUp</h2>
                    <input type="text" className="username" placeholder="Username"></input>
                    <input type="password" className="password" placeholder="Password"></input>
                    <input type="password" className="confpassword" placeholder="Confirm Password"></input>
                    <Button text="SignUp"/>
                    <p className="Para">Already have an account?  <a href="/login">Log in</a></p>
                </form>
            </div>
            <div className= "signupimage">
                <img className="keyimg" src={Key}/>
            </div>
        </div>
    );
}


export default SignUp;