import React from 'react';
import './navbar.css';

function Navbar() {
  return (
    <div className="Navbar">
      <a className="text" href="/">Home</a>&nbsp;
      <a className="text" href="/#how-it-works">How it works</a>&nbsp;
      <a className="text" href="/#about-us">About us</a>
      <a href='/login'>
        <button type="button" name="login" className="login">
          Login
        </button>
      </a>
    </div>
  );
}

export default Navbar;
