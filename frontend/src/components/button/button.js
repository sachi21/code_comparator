import React from 'react';
import './button.css';

function Button({text}) {
    return(
        <div className="ButtonTemplate">
            <a href='/editor'><button className="button">{text}</button></a>
            <div  className="shadow"> </div>
        </div>
        
    )
}

export default Button;