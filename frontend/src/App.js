import React from 'react';

import { BrowserRouter, Routes, Route } from "react-router-dom";

import Navbar from './components/navbar/navbar';
import EditorPage from './containers/editor/editor';
import HomePage from "./containers/HomePage/homepage"
import Login from './containers/login/login';
import SignUp from './containers/signup/signup'
import Projects from './containers/Projects/projects'

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<HomePage />} />        
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/editor" element={<EditorPage />} />
        <Route path="/projects" element={<Projects />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
